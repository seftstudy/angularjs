var express    = require('express');
var app        = express();
var server     = require('http').createServer(app);
var port       = process.env.PORT || 3000;
var compress   = require('compression');
var bodyParser = require('body-parser');

app.use(compress());  
app.use(bodyParser.json());
app.use(bodyParser.json({type:'application/vnd.api+json'}));
app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static(__dirname + '/build'));

app.set('view engine', 'jade');

// var routes = require('./back-end/route');
// routes(app);

var connect     = require('connect');
var serveStatic = require('serve-static');
var vhost       = require('vhost');

var mainapp = connect();
var userapp = connect();

userapp.use(function(req, res, next){
  var username = req.vhost[0];
  req.originalUrl = req.url;
  req.url = '/' + username + req.url;

  next();
})


userapp.use(serveStatic('front-end'));

var app = connect();
app.use(vhost('lasa.me', mainapp));

server.listen(port);
console.log('Server is running on '+ port);
exports = module.exports=app;


