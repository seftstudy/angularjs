angular.module( 'webApp', [
    /*  Material Design */

    /*  Page */
    'home',
    'manage',

    /*  App */
    'app',
    'header',
    'footer'
  ]
)
