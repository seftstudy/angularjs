const BAG_LIST = [
  {
    id: 1,
    item: 1,
    focus: 0,
    image: './style/image/home/1.png'
  },
  {
    id: 2,
    item: 2,
    focus: 0,
    image: './style/image/home/2.png'
  },
  {
    id: 3,
    item: 3,
    focus: 0,
    image: './style/image/home/3.png'
  },
  {
    id: 4,
    item: 4,
    focus: 0,
    image: './style/image/home/4.png'
  },
  {
    id: 5,
    item: 5,
    focus: 0,
    image: './style/image/home/5.png'
  }
]

class homeController {
  constructor($scope, $state, $timeout){
    this.$scope         = $scope;
    this.$state         = $state;
    this.$timeout         = $timeout;
    this.$scope.bagList = BAG_LIST;

    $scope.naviagteManage = () => {
      this.focusAndNavigate();
    }

    $scope.rotateImage = (_bag) => {
      console.log(_bag.item);
      switch (_bag.item) {
        case 1:
          $scope.slideImage('left');
          this.$timeout(() => {
            $scope.slideImage('left')
          }, 300);
          break;

        case 2:
          $scope.slideImage('left');
          break;

        case 3:
          this.focusAndNavigate();
          break;

        case 4:
          $scope.slideImage('right');
          break;

        case 5:
          $scope.slideImage('right');
          this.$timeout(() => {
            $scope.slideImage('right')
          }, 300);
          break;
      }
    }

    $scope.slideImage = (_direction) => {
      _direction = 'left' === _direction ? 1 : -1;
      this.$scope.bagList = this.$scope.bagList.map(bag => {
        bag.item = bag.item + _direction;
        bag.item = 6 === bag.item ? 1 : bag.item;
        bag.item = 0 === bag.item ? 5 : bag.item;
        return bag;
      })

    }
  }

  focusAndNavigate() {
    this.$scope.bagList = this.$scope.bagList.map(bag => {
      if (3 === bag.item) {
        bag.focus = 1;
        localStorage.setItem('bag-selected', JSON.stringify(bag));
      } else {
        bag.focus = 2;
      }
      return bag;
    });

    this.$timeout(() => {
      this.$state.go('manage')
    }, 300);
  }
}

angular
  .module('home', ['ui.router'])
  .config([
    '$stateProvider',
    '$urlRouterProvider',
    '$locationProvider',
    ($stateProvider, $urlRouterProvider, $locationProvider) => {
      $stateProvider
      .state('home', {
        url: "/",
        template: '<home></home>'
      })
    }
  ])
  .directive('home', [() => {
    return {
      restrict: 'E',
      templateUrl : './page/home/home.html',
      transclude  : true,
      controller  : homeController
    }
  }]);