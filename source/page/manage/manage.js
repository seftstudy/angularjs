class manageController {
  constructor($scope) {
    this.$scope = $scope;
  }
}
angular
  .module('manage', ['ui.router'])
  .config([
    '$stateProvider',
    '$urlRouterProvider',
    '$locationProvider',
    ($stateProvider, $urlRouterProvider, $locationProvider) => {
      $stateProvider
      .state('manage', {
        url: "/manage",
        template: '<manage></manage>'
      })
    }
  ])
  .directive('manage', [() => {
    return {
      restrict: 'E',
      templateUrl : './page/manage/manage.html',
      transclude  : true,
      controller  : manageController
    }
  }]);