class headerController {}

angular
  .module('header', [])
  .directive('header', () => {
    return {
      restrict: 'E',
      templateUrl : './module/header/header.html',
      transclude  : true,
      controller  : headerController
    }
  })