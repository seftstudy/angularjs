class appController {}

angular
  .module( 'app', ['ui.router'])
  .config([
    '$stateProvider',
    '$urlRouterProvider',
    '$locationProvider',
    ($stateProvider, $urlRouterProvider, $locationProvider) => {
      /*  404 */
      $urlRouterProvider.otherwise("/");
      $locationProvider.html5Mode({
        enabled: true,
        requireBase: true
      });

      $locationProvider.hashPrefix('!');
    }
  ])

  .directive('app', () => {
    return {
      restrict: 'E',
      templateUrl : './module/app/app.html',
      transclude  : true,
      controller  : appController
    }
  })

  .filter('unitTimeConverter', () => {
    return (unixTimeStamp) => {
      var newDate = new Date(unixTimeStamp * 1000);
      return [
        newDate.getDate(),
        '/',
        newDate.getMonth() + 1,
        '/',
        newDate.getFullYear()
      ].join("");
    }
  })

  .filter('unitTimeConverterToHour', () => {
    return (unixTimeStamp) => {
      var newDate = new Date(unixTimeStamp * 1000);
      return [
        newDate.getHours(),
        ':',
        newDate.getMinutes()
      ].join("");
    }
  })