'use strict';

var
  gulp         = require('gulp'),
  jade         = require('gulp-jade'),
  sass         = require('gulp-sass'),
  useref       = require('gulp-useref'),
  browserSync  = require('browser-sync').create(),
  reload       = browserSync.reload,
  uglify       = require('gulp-uglify'),
  gulpIf       = require('gulp-if'),
  runSequence  = require('run-sequence'),
  copy         = require('gulp-contrib-copy'),
  data         = require('gulp-data'),
  del          = require('del'),
  concat       = require('gulp-concat'),
  minifyCSS    = require('gulp-minify-css'),
  autoprefixer = require('gulp-autoprefixer'),
  ngAnnotate   = require('gulp-ng-annotate'),
  babel        = require('gulp-babel'),
  nodemon      = require('gulp-nodemon'),
  eslint       = require('gulp-eslint');

/*  GULP SASS */
/*  Vendor */
var listScssVendor = [
  './source/style/scss/reset.scss',
];

var chec
gulp.task('sass-vendor', function () {
  return gulp.src(listScssVendor)
    .pipe(sass().on('error', sass.logError))
    .pipe(minifyCSS())
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
    .pipe(concat('vendor.min.css'))
    .pipe(gulp.dest('./build/style/css/'))
});

/*  Style */
var listScssStyle = [
  './source/style/scss/layout.scss',
  './source/style/scss/component.scss',
  './source/page/**/*.scss',
  './source/module/**/*.scss'
];
gulp.task('sass-style', function () {
  return gulp.src(listScssStyle)
    .pipe(sass().on('error', sass.logError))
    .pipe(minifyCSS())
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
    .pipe(concat('style.min.css'))
    .pipe(gulp.dest('./build/style/css/'))
    .pipe(reload({stream: true}));
});

/*  GULP JS */
/*  Vendor */
var listJsVendor = [
  './node_modules/angular/angular.min.js',
  './node_modules/angular-ui-router/release/angular-ui-router.min.js',
];

gulp.task('js-vendor', function () {
  return gulp.src(listJsVendor)
    .pipe(ngAnnotate())
    .pipe(eslint({
      rules: {
        'my-custom-rule': 1,
        'strict': 2
      },
      globals: [
        'jQuery',
        '$'
      ],
      envs: [
        'browser'
      ]
    }))
    .pipe(uglify())
    .pipe(concat('vendor.min.js'))
    .pipe(gulp.dest('./build/style/js/'))
});

/*  Script */
var listJsScript = [
  './source/style/js/angular-application.js',
  './source/page/**/*.js',
  './source/module/**/*.js',
];

gulp.task('js-script', function () {
  return gulp.src(listJsScript)
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(ngAnnotate())
    .pipe(concat('script.min.js'))
    .pipe(gulp.dest('./build/style/js/'))
    .pipe(reload({stream: true}));
});

gulp.task('js-script-minify', function () {
  return gulp.src(listJsScript)
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(concat('script.min.js'))
    .pipe(gulp.dest('./build/style/js/'))
    .pipe(reload({stream: true}));
});

/*  GULP JADE */
gulp.task('jade', function() {
  var locals = {};
  var j = jade({
    locals: locals,
    pretty: true
  }).on('error', function(err) {
    console.log(err);
    j.end();});

    return gulp.src('./source/**/*.jade')
      .pipe(j)
      .pipe(gulp.dest('./build'))
      .pipe(reload({stream: true}));
  }
);

/*  GULP COPY FILE */
var listAsset = [
  './source/**/*.png',
  './source/**/*.jpg',
  './source/**/*.svg',
  './source/**/*.eot',
  './source/**/*.ttf',
  './source/**/*.woff',
  './source/**/*.woff2',
  './source/**/*.otf'
];
gulp.task('copy-asset', function () {
  gulp.src(listAsset)
    .pipe(gulp.dest('./build'));
});

/* Build vendor for Dev */
gulp.task('dev-vendor', ['sass-vendor', 'js-vendor'], function(){});

/* Build not vendor for Dev */
gulp.task('dev-not-vendor', ['jade', 'sass-style', 'js-script', 'copy-asset'], function() {
  browserSync.init({
    server: './build'
  });

  gulp.watch(['./source/**/*.jade'], ['jade']);
  gulp.watch(['./source/**/*.scss'], ['sass-style']);
  gulp.watch(['./source/**/*.js'], ['js-script']);
  gulp.watch(['./source/**/*.png', './source/**/*.jpg', './source/**/*.svg'], ['copy-asset']);

  console.log('-------------------------');
  console.log('| DEVELOP ENV COMPLETED |');
  console.log('-------------------------');
});

gulp.task('start', function(){
  runSequence('dev-vendor', 'dev-not-vendor', function() {
    console.log('---------------------------');
    console.log('| START PROJECT COMPLETED |');
    console.log('---------------------------');
  });
});

gulp.task('build', ['sass-vendor', 'js-vendor', 'jade', 'sass-style', 'js-script-minify', 'copy-asset'], function(){
  console.log('----------------------------');
  console.log('| PRODUCTION ENV COMPLETED |');
  console.log('----------------------------');
});

